import React from "react";
import ReactDOM from "react";
import MainContainer from "../components/MainContainer";
import { Formik, Field, Form} from "formik";

function App() {
    return (
    <MainContainer>
      <div className="Password-reset">
        <h1>Reinicio de contraseña</h1>
        <Formik
          initialValues={{ newPassword: "", confirmPassword: "" }}
          onSubmit={async values => {
            
          }}
        >
          <Form>
            <div id="setNewPassword">
            <h2>Ingrese nueva contraseña</h2>
            <Field type="password" id="newPassword"/>
              </div>
              <div id="ConfirmNewPassword">
            <h2>Ingrese nueva contraseña</h2>
            <Field type="password" id="confirmPassword"/>
              </div>
            
            <button type="submit">Reinicar Contraseña</button>
            
          </Form>
        </Formik>
      </div>
    </MainContainer>
    );
}

import React, { FC } from "react"
import MainContainer from "../../components/MainContainer"
import Field from "../../components/Field"
import Button from "../../components/Button"
import { FormikHelpers, Formik } from "formik"
import * as Yup from "yup"
import {
  passwordValidationYup,
  emailValidationYup,
} from "../../util/validations"
import { FormStyles } from "../../util/Styles"
import { Box, Grid, makeStyles } from "@material-ui/core"
import { handleSignIn } from "../../services/auth"
import { toast } from "react-toastify"
const signInSchema = Yup.object({
  email: emailValidationYup,
  password: passwordValidationYup,
})

interface Values {
  email: string
  password: string
}

const FormStyle = FormStyles;

const useStyle = makeStyles({
  H1Style: {
    textAlign: "center",
    marginTop: "0rem",
  },
})

const SignIn: FC = () => {
  const classes = useStyle()
  return (
    <MainContainer title={"Iniciar Sesión"}>
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validationSchema={signInSchema}
        onSubmit={({ email, password }: Values, {}: FormikHelpers<Values>) => {
          return handleSignIn(email, password).then(res => {
            if (res.problem) {
              const msg = "ola soi maria borra mi otro num"
              toast.error(msg, { toastId: msg })
              return
            }

            toast.success("Welcome")
            return
          })
        }}
      >
        {({ errors }) => {
          return (
            <FormStyle style={{  
              height: "350px",
              padding: "30px", 
              marginTop: "7rem",
              borderTop: "0rem",
              }}>
              <Grid item xs={10} sm={6}>
                <Box
                  flexDirection="column"
                  display="flex"
                  justifyContent="center"
                  alignContent="center"
                >
                  <h1 className={classes.H1Style} color="secondary">Iniciar sesión</h1>

                  <Field
                    errorMsg={errors.email}
                    name="email"
                    type="email"
                    label="Email"
                  />

                  <Field
                    errorMsg={errors.password}
                    name="password"
                    type="password"
                    label="Contraseña"
                  />
                  <Button type="submit" color="secondary">Iniciar sesión</Button>
                </Box>
              </Grid>
            </FormStyle>
          )
        }}
      </Formik>
    </MainContainer>
  )
}

export default SignIn

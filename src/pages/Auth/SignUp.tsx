import React, { FC } from "react"
import MainContainer from "../../components/MainContainer"
import Field from "../../components/Field"
import Button from "../../components/Button"
import { Formik, FormikHelpers } from "formik"
import * as Yup from "yup"
import {
  passwordValidationYup,
  emailValidationYup,
} from "../../util/validations"
import { FormStyles } from "../../util/Styles"
import { Box, Grid, makeStyles } from "@material-ui/core"

const formSchema = Yup.object().shape({
  name: Yup.string().required("Campo Requerido"),
  lastName: Yup.string().required("Campo Requerido"),
  email: emailValidationYup,
  password: passwordValidationYup,
  confirmPassword: Yup.string().oneOf(
    [Yup.ref("password")],
    "Las contraseñas deben ser iguales"
  ),
})

interface Values {
  name: string
  lastName: string
  email: string
  password: string
  confirmPassword: string
}

const FormStyle = FormStyles;

const useStyle = makeStyles({
  H1Style: {
    textAlign: "center",
    marginTop: "0rem",
  },
})

const SignUp: FC = () => {
  const classes = useStyle()

  return (
    <MainContainer title="Crear Cuenta">
      <Formik
        initialValues={{
          name: "",
          lastName: "",
          email: "",
          password: "",
          confirmPassword: "",
        }}
        validationSchema={formSchema}
        onSubmit={(
          values: Values,
          { setSubmitting }: FormikHelpers<Values>
        ) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2))
            setSubmitting(false)
          }, 500)
        }}
      >
        {({ errors }) => {
          return (
          <FormStyle style={{marginTop: "5rem",}}>
            <Grid item xs={10} sm={6}>
              <Box
                flexDirection="column"
                display="flex"
                justifyContent="center"
                alignContent="center"
              >
                <h1 className={classes.H1Style} color="secondary">Registrarse</h1>
                <Field errorMsg={errors.name} label="Nombre" name="name" />

                <Field
                  errorMsg={errors.lastName}
                  label="Apellido"
                  name="lastName"
                />

                <Field
                  errorMsg={errors.email}
                  label="Email"
                  name="email"
                  type="email"
                />

                <Field
                  errorMsg={errors.password}
                  label="Contraseña"
                  name="password"
                  type="password"
                />

                <Field
                  errorMsg={errors.confirmPassword}
                  label="Confirme Contraseña"
                  name="password"
                  type="password"
                />

                <Button type="submit" color="secondary">Registrarse</Button>
              </Box>
            </Grid>
          </FormStyle>
          )
        }}
      </Formik>
    </MainContainer>
  )
}

export default SignUp

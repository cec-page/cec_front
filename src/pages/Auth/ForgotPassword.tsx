import React, { FC } from "react"
import MainContainer from "../../components/MainContainer"
import Field from "../../components/Field"
import Button from "../../components/Button"
import { FormikHelpers, Formik } from "formik"
import * as Yup from "yup"
import {
  emailValidationYup,
} from "../../util/validations"
import { FormStyles } from "../../util/Styles"
import { Box, Grid, makeStyles } from "@material-ui/core"

const forgotPasswordSchema = Yup.object({
  email: emailValidationYup,
})

interface Values {
  email: string
}

const FormStyle = FormStyles;

const useStyle = makeStyles({
    H1Style: {
      textAlign:'center',
      marginTop: '0rem',
    },
});

const ForgotPassword: FC = () => {
  const classes = useStyle();
  return (
    <MainContainer title={"Contraseña olvidada"}>
      <Formik
        initialValues={{
          email: "",
        }}
        validationSchema={forgotPasswordSchema}
        onSubmit={(
          values: Values,
          { setSubmitting }: FormikHelpers<Values>
        ) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2))
            setSubmitting(false)
          }, 500)
        }}
      >
        {({ errors }) => {
          return (
            <FormStyle style={{  
              marginTop: '7rem',
              borderTop: '0rem',
              width: '400px',
              height: '300px',}}>
              <Grid item xs={10} sm={9}>
                <Box
                  flexDirection="column"
                  display="flex"
                  justifyContent="center"
                  alignContent="center"
                >
                  <h1 className={classes.H1Style}>Recuperar contraseña</h1>
                  <Field
                    errorMsg={errors.email}
                    name="email"
                    type="email"
                    label="Email"
                  />
                  <Button type="submit" color="secondary">Aceptar</Button>
                </Box>
                </Grid>
            </FormStyle>
          )
        }}
      </Formik>
    </MainContainer>
  )
}

export default ForgotPassword

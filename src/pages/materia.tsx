import React, { FC } from "react"
import { graphql } from "gatsby"
import MainContainer from "../components/MainContainer/index"
import Field from "../components/Field"
import Button from "../components/Button"
interface SubjectPageProps {
  data: any
}

export default function Subject(props: SubjectPageProps) {
  const { data } = props
  // const { name, description } = data
  const materias: any[] = data.allSubjects.nodes

  return (
    <MainContainer title={"Subject Page"}>
      <p>
        We're the only site running on your computer dedicated to showing the
        best photos and videos of pandas eating lots of food.
      </p>

      {materias.map(materia => (
        <div key={name}>
          <h5>{materia.name}</h5>
          <p>{materia.description}</p>
        </div>
      ))}
    </MainContainer>
  )
}

export const query = graphql`
  query {
    allSubjects {
      nodes {
        description
        name
        _id
      }
    }
  }
`

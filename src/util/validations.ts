import * as Yup from "yup"

export const passwordValidationYup = Yup.string()
  .required("Contraseña Requerida.")
  .matches(/[A-Z].{0,}/, "Debe contener al menos una mayúscula")
  .matches(/(?=.*[a-z]).{0,}/, "Debe contener al menos una minuscula")
  .matches(/(?=.*\d).{0,}$/, "Debe contener al menos un número")
  .matches(/(?=.*[@$!%*#?&_]).{0,}/, "Debe contener al menos un símbolo.")
  .min(6, "Mínimo 6 caracteres.")
  .max(15, "Máximo 15 caracteres.")
export const emailValidationYup = Yup.string()
  .required("Email requerido.")
  .email("Correo Electronico Invalido")

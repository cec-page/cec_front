import { createMuiTheme } from "@material-ui/core/styles";
import { teal, blue } from "@material-ui/core/colors";

const ThemePage = createMuiTheme({
  palette: {
    type: "light",
    primary: {
      main: teal[600],
    },
    secondary: {
      main: blue[900]
    },
  },
  overrides: {
    MuiButton: {
      root: {
        width: "100%",
        border: "none",
        padding: "12px",
        color: "white",
        margin: "16px 0",
        fontSize: "16px",
        marginTop: "25px",
        marginBottom: "5px",
      },
    },
  },
});

export default ThemePage;

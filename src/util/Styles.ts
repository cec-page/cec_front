import { styled } from "@material-ui/core"
import { Form } from "formik"

export const FormStyles = styled(Form)({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginTop: "3rem",
  borderTop: "0rem",
  width: "450px",
  height: "500px",
  padding: "30px",
  margin: "auto",
  borderRadius: "4px",
  fontFamily: "calibri",
  boxShadow: "7px 13px 37px #000",
})
import React, { FC } from "react"
import { TextField as MuiTextField } from "formik-material-ui"
import { TextFieldProps as MuiTextFieldProps } from "@material-ui/core/TextField"
import { Field as FField } from "formik"
export type FieldProps = MuiTextFieldProps

const Field: FC<FieldProps & { errorMsg: string | undefined }> = props => {
  const passedProps = { ...props }
  const errorMsg = props.errorMsg
  delete passedProps.errorMsg
  return (
    <FField
      error={errorMsg ? true : false}
      helperText={errorMsg}
      {...passedProps}
      component={MuiTextField}
    />
  )
}

export default Field

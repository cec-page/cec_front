import React, { FC } from "react"
import * as classes from "./MainContainer.module.scss"
import Box from "@material-ui/core/Box"
import NavBar from "../NavBar/index"
import { ToastContainer } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"
import { Helmet } from 'react-helmet'
import { ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import ThemePage from "../../util/theme"

const MainContainer: FC<{title:string}> = ({ title=' PortalaCEC', children }) => {

  return (
    <ThemeProvider theme={ThemePage}>
    <CssBaseline />
    <Box
      style={{ padding: 0, margin: 0, minHeight: "100vh" }}
      className={classes.red}
      color="secondary"
    >
      <Helmet title={title} defer={false} />
      {children}
      <NavBar mode="admin" />
      <ToastContainer
        position="top-center"
        autoClose={5000}
        newestOnTop={true}
        closeOnClick
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </Box>
    </ThemeProvider>
  )
}
export default MainContainer

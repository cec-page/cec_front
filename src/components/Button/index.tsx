import React, { FC } from "react"
import MuiButton, {
  ButtonProps as MuiButtonProps,
} from "@material-ui/core/Button"

interface ButtonProps extends MuiButtonProps {}

const Button: FC<ButtonProps> = props => {
  return (
    <MuiButton variant="contained" {...props}>
      {props.children}
    </MuiButton>
  )
}

export default Button

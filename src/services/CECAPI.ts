import { create } from "apisauce"

export const CAPI_URL = 
  process.env.BACKEND_URL
const caxios = create({
  baseURL: CAPI_URL,
})
const capi = {
  Auth: {
    SignUp(email: string, password: string, name: string) {
      return caxios.post("/sign-up", {
        email,
        password,
        name,
      })
    },
    SignIn(email: string, password: string) {
      return caxios.post("/sign-in", {
        email,
        password,
      })
    },
  },
}

export default capi

import capi from "./CECAPI"

export const isBrowser = () => typeof window !== "undefined"

export const getUser = () =>
  isBrowser() && window.localStorage.getItem("gatsbyUser")
    ? JSON.parse(window.localStorage.getItem("gatsbyUser") || "")
    : {}

const setUser = (user: any) =>
  window.localStorage.setItem("gatsbyUser", JSON.stringify(user))

export const handleSignIn = (email: string, password: string) => {
  return capi.Auth.SignIn(email, password).then(res => {
    console.log(res)
    return res
  })
}

export const handleSignUp = (email: string, password: string, name: string) => {
  return capi.Auth.SignUp(email, password, name).then(res => {
    console.log(res)
    return res
  })
}

export const isLoggedIn = () => {
  const user = getUser()

  return !!user.username
}

export const logout = (callback: any) => {
  setUser({})
  callback()
}

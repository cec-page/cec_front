/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */
const activeEnv =
  process.env.GATSBY_ACTIVE_ENV || process.env.NODE_ENV || "development"
console.log(`Using environment config: '${activeEnv}'`)

require("dotenv").config({
  path: `.env.${activeEnv}`,
})

const CECAPI_ENDPOINT = endpoint => process.env.BACKEND_URL + endpoint
const path = require("path")
console.log(CECAPI_ENDPOINT("/subject"))
module.exports = {
  /* Your site config here */
  // in gatsby-config.js
  plugins: [
    {
      resolve: "gatsby-plugin-scss-typescript",
      options: {
        cssLoaderOptions: {
          importLoaders: 1,
          localIdentName: "[name]_[local]___[hash:base64:5]_[emoji:1]",
        },
        sassLoaderOptions: {
          test: /\.s[ac]ss$/i,
          includePaths: [path.resolve(__dirname, "./src/")],
        },
        cssMinifyOptions: {
          assetNameRegExp: /\.optimize\.css$/g,
          canPrint: true,
        },
        cssExtractOptions: {
          filename: "[name].css",
          chunkFilename: "[id].css",
        },
      },
    },
    {
      resolve: "gatsby-plugin-root-import",
      options: {
        src: path.join(__dirname, "src/"),
      },
    },
    {
      resolve: "gatsby-plugin-react-helmet",
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `PortalaCEC`,
        short_name: `PortalaCEC`,
        start_url: `/`,
        background_color: `#f7f0eb`,
        theme_color: `#a2466c`,
        display: `standalone`,
      },
    },
    {
      resolve: `gatsby-plugin-offline`,
      options: {
        precachePages: [],
      },
    },
    {
      resolve: "gatsby-source-custom-api",
      options: {
        url: {
          development: CECAPI_ENDPOINT("/subject"), // on "gatsby develop"
          // production: "https://my-remote-api.com", // on "gatsby build"
          
        },
        // imageKeys: ["images"],
        rootKey: "subjects",
        schemas: {
          subject: `
          name: String
          description: String
          _id:String
          
          `,
          subjects: `


            payload: [subject]
          `,
        },
      },
    },
  ],
  siteMetadata: {
    title: `PortalaCEC`,
  },
}
